//sempre quando importar a biblioteca express executar a sua função


var express = require('express');
var app = express();
var consign = require('consign');




//indicamos para o express que o ejs sera nosso geração de views da aplicação
app.set('view engine', 'ejs');

// indicamos para o express onde de se encontram as view da aplicação
// lembrando que o path dos view tera que ser relativo a onde sera importado a configuração
// neste caso a partir do config/server.
app.set('views', './app/views');


//injetamos os modulos das rotas automaticamente quando inicie o servidor
consign()
    .include('app/routes')
    .then('config/dbConection.js')
    .into(app);

module.exports = app;